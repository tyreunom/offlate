<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS language="it"><context>
    <name>AboutWindow</name>
    <message>
        <location filename="../ui/about.py" line="45" />
        <source>Offlate is a translation interface for offline translation of projects using online platforms. Offlate is free software, you can redistribute it under the GPL v3 license or any later version.</source>
        <translation>Offlate &#232; un'interfaccia per la traduzione offline di progetti che usano piattaforme online. Offlate &#232; software libero, puoi ridistribuirlo sotto la licenza GPL v3 o successive.</translation></message>
    <message>
        <location filename="../ui/about.py" line="49" />
        <source>Copyright (C) 2018, 2019 Julien Lepiller</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/about.py" line="53" />
        <source>Report an issue</source>
        <translation>Segnala un problema</translation></message>
    <message>
        <location filename="../ui/about.py" line="54" />
        <source>Close this window</source>
        <translation>Chiudi questa finestra</translation></message>
    <message>
        <location filename="../ui/about.py" line="51" />
        <source>Copyright (C) 2018-2021 Julien Lepiller</source>
        <translation>Copyright (C) 2018-2021 Julien Lepiller</translation></message>
</context>
<context>
    <name>AdvancedProjectWidget</name>
    <message>
        <location filename="../ui/new.py" line="94" />
        <source>Project information</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="102" />
        <source>Name:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="103" />
        <source>Target Language:</source>
        <translation type="obsolete" />
    </message>
</context>
<context>
    <name>CopyrightSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="139" />
        <source>In some cases, we need to add a copyright line for you in the translation files we send upstream. Here, you can configure how you want the copyright line to look like. This information will most likely become public after your first contribution.</source>
        <translation>In alcuni casi dobbiamo aggiungere una riga di copyright a tuo nome nei file che mandiamo upstream. Qui puoi configurare che aspetto vuoi dare alla riga di copyright. Quest'informazione diventer&#224; probabilmente pubblica dopo il tuo primo contributo.</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="186" />
        <source>John Doe</source>
        <translation>Mario Rossi</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="188" />
        <source>john@doe.me</source>
        <translation>mario@rossi.me</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="160" />
        <source>Here is how your copyright line will look like:</source>
        <translation>Ecco come apparir&#224; la tua riga di copyright:</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="189" />
        <source>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</source>
        <translation>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</translation></message>
</context>
<context>
    <name>EditorWindow</name>
    <message>
        <location filename="../ui/editor.py" line="551" />
        <source>Unsupported / Unknown project</source>
        <translation>Progetto sconosciuto / Non supportato</translation></message>
    <message numerus="yes">
        <location filename="../ui/editor.py" line="573" />
        <source>{} translated on {} total ({}%).</source>
        <translation><numerusform>{} tradotta su {} totali ({}%).</numerusform><numerusform>{} tradotte su {} totali ({}%).</numerusform></translation></message>
    <message>
        <location filename="../ui/editor.py" line="672" />
        <source>Exit</source>
        <translation>Esci</translation></message>
    <message>
        <location filename="../ui/editor.py" line="674" />
        <source>Exit application</source>
        <translation>Esci dall'applicazione</translation></message>
    <message>
        <location filename="../ui/editor.py" line="677" />
        <source>Save</source>
        <translation>Salva</translation></message>
    <message>
        <location filename="../ui/editor.py" line="679" />
        <source>Save current project</source>
        <translation>Salva il progetto corrente</translation></message>
    <message>
        <location filename="../ui/editor.py" line="682" />
        <source>New</source>
        <translation>Nuovo</translation></message>
    <message>
        <location filename="../ui/editor.py" line="684" />
        <source>New project</source>
        <translation>Nuovo progetto</translation></message>
    <message>
        <location filename="../ui/editor.py" line="687" />
        <source>Manage Projects</source>
        <translation>Gestisci Progetti</translation></message>
    <message>
        <location filename="../ui/editor.py" line="690" />
        <source>Open project manager</source>
        <translation>Apri il gestore progetti</translation></message>
    <message>
        <location filename="../ui/editor.py" line="693" />
        <source>Update</source>
        <translation>Aggiorna</translation></message>
    <message>
        <location filename="../ui/editor.py" line="695" />
        <source>Get modifications from upstream</source>
        <translation>Ricevi modifiche da upstream</translation></message>
    <message>
        <location filename="../ui/editor.py" line="698" />
        <source>Close</source>
        <translation>Chiudi</translation></message>
    <message>
        <location filename="../ui/editor.py" line="699" />
        <source>Close current project</source>
        <translation>Chiudi il progetto corrente</translation></message>
    <message>
        <location filename="../ui/editor.py" line="702" />
        <source>Send</source>
        <translation>Invia</translation></message>
    <message>
        <location filename="../ui/editor.py" line="704" />
        <source>Send modifications upstream</source>
        <translation>Invia le modifiche a upstream</translation></message>
    <message>
        <location filename="../ui/editor.py" line="707" />
        <source>Settings</source>
        <translation>Impostazioni</translation></message>
    <message>
        <location filename="../ui/editor.py" line="709" />
        <source>Set parameters</source>
        <translation>Imposta parametri</translation></message>
    <message>
        <location filename="../ui/editor.py" line="722" />
        <source>Show Translated</source>
        <translation>Mostra Traduzioni</translation></message>
    <message>
        <location filename="../ui/editor.py" line="725" />
        <source>Show Fuzzy</source>
        <translation>Mostra Fuzzy</translation></message>
    <message>
        <location filename="../ui/editor.py" line="728" />
        <source>Show Empty Translation</source>
        <translation>Mostra Traduzioni Vuote</translation></message>
    <message>
        <location filename="../ui/editor.py" line="731" />
        <source>Use a monospace font</source>
        <translation>Usa un carattere monospace</translation></message>
    <message>
        <location filename="../ui/editor.py" line="756" />
        <source>Open</source>
        <translation>Apri</translation></message>
    <message>
        <location filename="../ui/editor.py" line="760" />
        <source>&amp;File</source>
        <translation>&amp;File</translation></message>
    <message>
        <location filename="../ui/editor.py" line="768" />
        <source>&amp;Project</source>
        <translation>&amp;Progetto</translation></message>
    <message>
        <location filename="../ui/editor.py" line="775" />
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation></message>
    <message>
        <location filename="../ui/editor.py" line="780" />
        <source>&amp;View</source>
        <translation>&amp;Mostra</translation></message>
    <message>
        <location filename="../ui/editor.py" line="585" />
        <source>Uploading {}...</source>
        <translation>Sto caricando {}...</translation></message>
    <message>
        <location filename="../ui/editor.py" line="592" />
        <source>Finished uploading {}!</source>
        <translation>Finito di caricare {}!</translation></message>
    <message>
        <location filename="../ui/editor.py" line="595" />
        <source>Updating {}...</source>
        <translation>Sto aggiornando {}...</translation></message>
    <message>
        <location filename="../ui/editor.py" line="607" />
        <source>Finished updating {}!</source>
        <translation>Finito di aggiornare {}!</translation></message>
    <message>
        <location filename="../ui/editor.py" line="712" />
        <source>Search</source>
        <translation>Cerca</translation></message>
    <message>
        <location filename="../ui/editor.py" line="714" />
        <source>Search in the document</source>
        <translation>Cerca nel documento</translation></message>
    <message>
        <location filename="../ui/editor.py" line="717" />
        <source>Replace</source>
        <translation>Sostituisci</translation></message>
    <message>
        <location filename="../ui/editor.py" line="719" />
        <source>Replace content in the document</source>
        <translation>Sostituisci contenuto nel documento</translation></message>
</context>
<context>
    <name>GitlabEdit</name>
    <message>
        <location filename="../ui/gitlabedit.py" line="59" />
        <source>server</source>
        <translation>server</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="61" />
        <source>token</source>
        <translation>token</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="62" />
        <source>Add</source>
        <translation>Aggiungi</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="64" />
        <source>Remove</source>
        <translation>Rimuovi</translation></message>
</context>
<context>
    <name>LangSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="204" />
        <source>Which language will you most frequently translate projects into? This setting can be overriden in each individual projects.</source>
        <translation>Di solito verso quale lingua traduci i progetti? Questa impostazione pu&#242; anche essere modificata per ciascun singolo progetto. </translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="211" />
        <source>None</source>
        <translation>Nessuna</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="229" />
        <source>Note that you should only translate into your native language or to a language you are extremely familiar with, to avoid weird or nonsensical translations.</source>
        <translation>Ricorda che dovresti solo tradurre verso la tua lingua madre o una lingua con cui hai completa dimestichezza, per evitare traduzioni bizzarre o prive di senso.</translation></message>
</context>
<context>
    <name>ListSettingsEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="142" />
        <source>Add</source>
        <translation>Aggiungi</translation></message>
    <message>
        <location filename="../ui/settingsedit.py" line="144" />
        <source>Remove</source>
        <translation>Rimuovi</translation></message>
</context>
<context>
    <name>ListSettingsRowEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="53" />
        <source>OK</source>
        <translation>OK</translation></message>
</context>
<context>
    <name>MultipleLineEdit</name>
    <message>
        <location filename="../ui/multiplelineedit.py" line="59" />
        <source>Add</source>
        <translation>Aggiungi</translation></message>
    <message>
        <location filename="../ui/multiplelineedit.py" line="61" />
        <source>Remove</source>
        <translation>Rimuovi</translation></message>
</context>
<context>
    <name>NewWindow</name>
    <message>
        <location filename="../ui/new.py" line="57" />
        <source>Project information</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="65" />
        <source>Name:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="66" />
        <source>Target Language:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="68" />
        <source>The Translation Project</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="69" />
        <source>Transifex</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="70" />
        <source>Gitlab</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="230" />
        <source>Cancel</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="231" />
        <source>OK</source>
        <translation>OK</translation></message>
    <message>
        <location filename="../ui/new.py" line="100" />
        <source>Organization</source>
        <translation type="obsolete">Organizzazione</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="122" />
        <source>repository</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="127" />
        <source>branch</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/new.py" line="71" />
        <source>Github</source>
        <translation type="obsolete" />
    </message>
</context>
<context>
    <name>PageCopyright</name>
    <message>
        <location filename="../ui/welcome.py" line="98" />
        <source>Copyright Settings</source>
        <translation>Impostazioni Copyright</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="99" />
        <source>Configuring how your copyright is added to files</source>
        <translation>Configurare come aggiungere il tuo copyright ai file</translation></message>
</context>
<context>
    <name>PageDownload</name>
    <message>
        <location filename="../ui/welcome.py" line="188" />
        <source>Project Fetch</source>
        <translation>Recupero Progetto</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="189" />
        <source>Downloading translation files</source>
        <translation>Scaricamento file di traduzione</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="192" />
        <source>We are now attempting to fetch the translation files of your project. This step should be pretty fast.</source>
        <translation>Stiamo provando a recuperare i file di traduzione del tuo progetto. Questo passaggio dovrebbe essere piuttosto rapido.</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="222" />
        <source>Finished!</source>
        <translation>Finito!</translation></message>
</context>
<context>
    <name>PageHints</name>
    <message>
        <location filename="../ui/welcome.py" line="302" />
        <source>You can always go back to the initial screen (called the project manager) from the editor, by using &lt;i&gt;file &gt; project manager&lt;/i&gt;</source>
        <translation>Puoi sempre tornare alla schemata iniziale (il gestore progetti) dall'editor, usando &lt;i&gt;file &gt; apri il gestore progetti&lt;/i&gt;</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="311" />
        <source>Quickly switch to the next string with &lt;i&gt;Ctrl+Enter&lt;/i&gt;. Use &lt;i&gt;Ctrl+Shift+Enter&lt;/i&gt; to go back to the previous string instead.</source>
        <translation>Spostati rapidamente alla stringa successiva con &lt;i&gt;Ctrl+Enter&lt;/i&gt;. Usa &lt;i&gt;Ctrl+Shift+Enter&lt;/i&gt; per tornare invece alla stringa precedente.</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="320" />
        <source>With these hints, you are now ready to start working on your project! We will now open the project manager, from which you can add more projects, change your settings, etc. Have fun!</source>
        <translation>Detto questo, puoi iniziare a lavorare sul tuo progetto! Ora apriremo il gestore progetti, da dove puoi aggiungere altri progetti, cambiare impostazioni, eccetera. Buon divertimento!</translation></message>
</context>
<context>
    <name>PageLang</name>
    <message>
        <location filename="../ui/welcome.py" line="117" />
        <source>Language Settings</source>
        <translation>Impostazioni Lingua</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="118" />
        <source>Configuring the default language for new projects</source>
        <translation>Configurare la lingua di default per nuovi progetti</translation></message>
</context>
<context>
    <name>PageProjectSelection</name>
    <message>
        <location filename="../ui/welcome.py" line="140" />
        <source>First Project</source>
        <translation>Primo Progetto</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="141" />
        <source>Choosing a first project</source>
        <translation>Scegliere il primo progetto</translation></message>
</context>
<context>
    <name>PageSuccess</name>
    <message>
        <location filename="../ui/welcome.py" line="279" />
        <source>Congratulations! We've just set up your first project! Initial setup is now complete, and you can always change the settings you've set here from the Offlate welcome screen. After a few explanations you will be able to work on your project right away!</source>
        <translation>Congratulazioni! Abbiamo appena configurato il tuo primo progetto! Il setup iniziale &#232; completo, e puoi sempre cambiare le impostazioni dalla schermata di benvenuto di Offlate. Dopo qualche spiegazione potrai subito iniziare a lavorare sul tuo progetto!</translation></message>
</context>
<context>
    <name>PageSystemSettings</name>
    <message>
        <location filename="../ui/welcome.py" line="163" />
        <source>System Configuration</source>
        <translation>Configurazione Sistema</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="164" />
        <source>Getting an account on the project's platform</source>
        <translation>Aprire un account sulla piattaforma del progetto</translation></message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../ui/welcome.py" line="81" />
        <source>Offlate is a tool to help you localise free and open source software. Before you start contributing translations to a project though, there are a few things we need to set up and talk about. Let's get started!</source>
        <translation>Offlate &#232; uno strumento che ti aiuta a localizzare software open e libero. Prima di iniziare a contribuire traduzioni a un progetto, per&#242;, ci sono un paio di cose da configurare e di cui parlare. Iniziamo!</translation></message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../systems/gitlab.py" line="92" />
        <source>Repository</source>
        <translation>Repository</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="92" />
        <source>Full clone URL for the repository</source>
        <translation>URL completo per il clone del repository</translation></message>
    <message>
        <location filename="../systems/github.py" line="89" />
        <source>https://...</source>
        <translation>https://...</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>Branch</source>
        <translation>Branch</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>Name of the branch to translate</source>
        <translation>Nome del branch da tradurre</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>master</source>
        <translation>master</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Token</source>
        <translation>Token</translation></message>
    <message>
        <location filename="../systems/github.py" line="98" />
        <source>You can get a token from &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;. You will need at least to grant the public_repo permission.</source>
        <translation>Puoi ottenere un token da &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;. Dovrai concedere almeno il permesso public_repo.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>Gitlab instance configuration</source>
        <translation>Configurazione dell'istanza Gitlab.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>You need to configure each Gitlab instance separately, and you haven't configured the instance at {} yet.</source>
        <translation>Devi configurare ogni istanza Gitlab separatamente, e non hai ancora configurato l'istanza {}.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>The token you created from your account. You can create it from &lt;a href="#"&gt;the Access Tokens tab&lt;/a&gt; in your account settings.</source>
        <translation>Il token che hai creato dal tuo account. Puoi crearlo dal &lt;a href="#"&gt;tab Access Tokens&lt;/a&gt; nelle impostazioni del tuo account.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="119" />
        <source>Configured Gitlab instances</source>
        <translation>Istanza Gitlab configurate</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="119" />
        <source>You need to create a token for each Gitlab instance you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation>Devi creare un token per ciascuna istanza Gitlab su cui hai un account. Puoi crearlo facendo login sul tuo account e andando nella pagina di impostazioni Access Token.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Server</source>
        <translation>Server</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Server name</source>
        <translation>Nome del server</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>The token you created from your account</source>
        <translation>Il token che hai creato dal tuo account</translation></message>
    <message>
        <location filename="../systems/tp.py" line="153" />
        <source>version</source>
        <translation>versione</translation></message>
    <message>
        <location filename="../systems/tp.py" line="153" />
        <source>version of the project (keep empty for latest)</source>
        <translation>versione del progetto (lascia vuoto per la pi&#249; recente)</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>Mail server</source>
        <translation>Server di posta</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>To send your work to the translation project on your behalf, we need to know the email server you are going to use (usually the part on the right of the `@` in your email address).</source>
        <translation>Per mandare il tuo lavoro al progetto di traduzione da parte tua, dobbiamo sapere il server di posta che vuoi usare (di solito &#232; la parte a destra della chiocciola nel tuo indirizzo email).</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>example.com</source>
        <translation>esempio.com</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>Mail user</source>
        <translation>Utente email</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>Username used to connect to your mail server, usually the email address itself, or the part of the left of the `@`.</source>
        <translation>Nome utente usato per connettersi al tuo server email, di solito &#232; l'indirizzo email stesso, o la parte a sinistra della chiocciola.</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>john</source>
        <translation>mario</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="147" />
        <source>You can get a token from &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153" />
        <source>Organization</source>
        <translation>Organizzazione</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="153" />
        <source>The organization this project belongs in, on transifex.</source>
        <translation>L'organizzazione a cui appartiene questo progetto, su transifex.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>Project</source>
        <translation>Progetto</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="155" />
        <source>The name of the project on transifex</source>
        <translation>Il nome del progetto su transifex</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>Instance</source>
        <translation>Istanza</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>https://weblate.org</source>
        <translation>https://weblate.org</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>Name of the project on the instance</source>
        <translation>Nome del progetto sull'istanza</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>foo</source>
        <translation>foo</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>Weblate instance configuration</source>
        <translation>Configurazione dell'istanza Weblate</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>You need to configure each Weblate instance separately, and you haven't configured the instance at {} yet.</source>
        <translation>Devi configurare ciascuna istanza Weblate separatamente, e non hai ancora configurato l'istanza {}.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>The token you created from your account. You can create it from &lt;a href="#"&gt;the API access tab&lt;/a&gt; in your account settings.</source>
        <translation>Il token che hai creato dal tuo account. Puoi crearlo dal &lt;a href#"#"&gt;tab API access&lt;/a&gt; nelle impostazioni del tuo account.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</source>
        <translation>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Configured Weblate instances</source>
        <translation>Istanze Weblate configurate</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>You need to find a token for each Weblate instance you have an account on. You can create a token by logging into your account, going to your settings and in the API Access page.</source>
        <translation>Devi trovare un token per ciascuna istanza Weblate su cui hai un account. Puoi creare un token facendo login sul tuo account e andando sulla pagina API Access delle impostazioni.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>URL of the Weblate instance</source>
        <translation>URL dell'istanza Weblate</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>https://hosted.weblate.org</source>
        <translation>https://hosted.weblate.org</translation></message>
</context>
<context>
    <name>ProjectManagerWidget</name>
    <message>
        <location filename="../ui/manager.py" line="89" />
        <source>Open</source>
        <translation>Apri</translation></message>
    <message>
        <location filename="../ui/manager.py" line="90" />
        <source>Edit</source>
        <translation>Modifica</translation></message>
    <message>
        <location filename="../ui/manager.py" line="91" />
        <source>Remove</source>
        <translation>Rimuovi</translation></message>
    <message>
        <location filename="../ui/manager.py" line="99" />
        <source>New Project</source>
        <translation>Nuovo Progetto</translation></message>
    <message>
        <location filename="../ui/manager.py" line="100" />
        <source>Settings</source>
        <translation>Impostazioni</translation></message>
    <message>
        <location filename="../ui/manager.py" line="101" />
        <source>About Offlate</source>
        <translation>Informazioni su Offlate</translation></message>
    <message>
        <location filename="../ui/manager.py" line="102" />
        <source>Exit</source>
        <translation>Esci</translation></message>
    <message>
        <location filename="../ui/manager.py" line="219" />
        <source>Fetching project {}...</source>
        <translation>Recupero il progetto {}...</translation></message>
</context>
<context>
    <name>ProjectManagerWindow</name>
    <message>
        <location filename="../ui/manager.py" line="53" />
        <source>Offlate Project Manager</source>
        <translation>Manager Progetti di Offlate</translation></message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <location filename="../ui/editor.py" line="159" />
        <source>Copy</source>
        <translation>Copia</translation></message>
    <message>
        <location filename="../ui/editor.py" line="303" />
        <source>Singular</source>
        <translation>Singolare</translation></message>
    <message>
        <location filename="../ui/editor.py" line="304" />
        <source>Plural</source>
        <translation>Plurale</translation></message>
    <message>
        <location filename="../ui/editor.py" line="155" />
        <source>Open in external editor</source>
        <translation>Apri nell'editor esterno</translation></message>
    <message>
        <location filename="../ui/editor.py" line="368" />
        <source>&lt;b&gt;location&lt;/b&gt;: {0} line {1}</source>
        <translation>&lt;b&gt;posizione&lt;/b&gt;: {0} riga {1}</translation></message>
    <message>
        <location filename="../ui/editor.py" line="370" />
        <source>&lt;b&gt;comment&lt;/b&gt;: {0}</source>
        <translation>&lt;b&gt;commento&lt;/b&gt;: {0}</translation></message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="../ui/search.py" line="84" />
        <source>String to search for</source>
        <translation>Stringa da cercare</translation></message>
    <message>
        <location filename="../ui/search.py" line="87" />
        <source>String to replace into</source>
        <translation>Stringa da sostituire</translation></message>
    <message>
        <location filename="../ui/search.py" line="92" />
        <source>Search</source>
        <translation>Cerca</translation></message>
    <message>
        <location filename="../ui/search.py" line="93" />
        <source>Replace</source>
        <translation>Sostituisci</translation></message>
    <message>
        <location filename="../ui/search.py" line="102" />
        <source>Case sensitive</source>
        <translation>Distingui maiuscolo</translation></message>
    <message>
        <location filename="../ui/search.py" line="103" />
        <source>Wrap around</source>
        <translation>A cavallo di righe</translation></message>
    <message>
        <location filename="../ui/search.py" line="105" />
        <source>Match whole word only</source>
        <translation>Solo parole intere</translation></message>
    <message>
        <location filename="../ui/search.py" line="106" />
        <source>Search in original text</source>
        <translation>Cerca nel testo originale</translation></message>
    <message>
        <location filename="../ui/search.py" line="108" />
        <source>Search in translated text</source>
        <translation>Cerca nel testo tradotto</translation></message>
    <message>
        <location filename="../ui/search.py" line="110" />
        <source>Search in comments</source>
        <translation>Cerca nei commenti</translation></message>
    <message>
        <location filename="../ui/search.py" line="122" />
        <source>Close</source>
        <translation>Chiudi</translation></message>
    <message>
        <location filename="../ui/search.py" line="126" />
        <source>Replace all</source>
        <translation>Sostituisci tutte</translation></message>
    <message>
        <location filename="../ui/search.py" line="128" />
        <source>Replace one</source>
        <translation>Sostituisci una</translation></message>
    <message>
        <location filename="../ui/search.py" line="130" />
        <source>&lt; Previous</source>
        <translation>&lt; Precedente</translation></message>
    <message>
        <location filename="../ui/search.py" line="132" />
        <source>Next &gt;</source>
        <translation>Seguente &gt;</translation></message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="287" />
        <source>Configure me</source>
        <translation>Configurami</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="308" />
        <source>Done!</source>
        <translation>Fatto!</translation></message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../ui/settings.py" line="42" />
        <source>Cancel</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="43" />
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="56" />
        <source>Transifex</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="68" />
        <source>You can get a token from &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="164" />
        <source>Token:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="86" />
        <source>Generic Settings</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="89" />
        <source>John Doe &lt;john@doe.me&gt;</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="96" />
        <source>Full Name:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="101" />
        <source>Generic</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="108" />
        <source>Translation Project</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="129" />
        <source>Email:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="130" />
        <source>Server:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="131" />
        <source>User Name:</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="178" />
        <source>Gitlab</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="180" />
        <source>Add your gitlab account tokens below. You need to create a token for every gitlab server you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="147" />
        <source>Github</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/settings.py" line="159" />
        <source>You can get a token from &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;.             You will need at least to grant the public_repo permission.</source>
        <translation type="obsolete" />
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="42" />
        <source>Offlate Settings</source>
        <translation>Impostazioni di Offlate</translation></message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="../ui/spellcheckedit.py" line="47" />
        <source>Spelling Suggestions</source>
        <translation>Suggerimenti Ortografici</translation></message>
    <message>
        <location filename="../ui/spellcheckedit.py" line="48" />
        <source>No Suggestions</source>
        <translation>Nessun Suggerimento</translation></message>
</context>
<context>
    <name>SystemSettingsWindow</name>
    <message>
        <location filename="../ui/config/settings.py" line="116" />
        <source>OK</source>
        <translation>OK</translation></message>
</context>
<context>
    <name>WelcomeWindow</name>
    <message>
        <location filename="../ui/config/welcome.py" line="38" />
        <source>Welcome to Offlate</source>
        <translation>Benvenuti in Offlate</translation></message>
</context>
<context>
    <name>dialog</name>
    <message>
        <location filename="../ui/main.py" line="60" />
        <source>The app just crashed, please report the following error, with any relevant information (what you did when the app crashed, any external factor that might be relevant, etc.). You can send your report on {}, or by email to {}.

{}
Traceback:
{}</source>
        <translation>L'applicazione si &#232; interrotta, per favore segnala il seguente errore, con ogni informazione rilevante (cosa stavi facendo, qualunque fattore esterno di rilievo, ecc.). Puoi mandare la segnalazione su {}, o via email a {}.&#8233;&#8233;{}&#8233;Traceback:&#8233;{}</translation></message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="../ui/parallel.py" line="57" />
        <source>A project with the same name already exists. The new project was not created. You should first remove the same-named project.</source>
        <translation>Esiste gi&#224; un progetto con lo stesso nome. Il nuovo progetto non &#232; stato creato. Dovresti prima rimuovere il progetto omonimo.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="61" />
        <source>Your filesystem contains a same-named directory for your new project. The new project was not created. You should first remove the same-named directory: "{}".</source>
        <translation>Il tuo filesystem contiene una directory con lo stesso nome del tuo nuovo progetto. Il nuovo progetto non &#232; stato creato. Dovresti prima rimuovere la directory omonima: "{}"</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="67" />
        <source>The project you added uses the {} format, but it is not supported yet by Offlate. You can try to update the application, or if you are on the latest version already, report it as a bug.</source>
        <translation>Il progetto che hai aggiunto usa il formato {}, ma non &#232; ancora supportato da Offlate. Puoi provare a aggiornare l'applicazione, o se sei gi&#224; all'ultima versione, segnalarlo come bug.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="75" />
        <source>An unexpected error occured while fetching the project: {}. You should report this as a bug.</source>
        <translation>Si &#232; verificato un errore imprevisto recuperando il progetto: {}. Dovresti segnalarlo come bug.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="71" />
        <source>The project {} you added could not be found in the translation platform you selected. Did you make a typo while entering the name or other parameters?</source>
        <translation>Il progetto {} che hai aggiunto non esiste sulla piattaforma di traduzione scelta. Forse hai scritto male il nome o altri parametri?</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="48" />
        <source>This action did not complete correctly. Try again, and if the issue persists, consider sending a bug report at {}, or by email to {} with the following information, and any relevant information (what you were trying to do, other external factors, etc.) We received the following error message: {}.

Traceback:

{}
Try again?</source>
        <translation>L'azione non &#232; andata a buon fine. Prova di nuovo, e se il problema persiste, manda una segnalazione di bug su {}, o via email a {} aggiungendo cosa stavi cercando di fare, altri fattori esterni, ecc. Abbiamo ricevuto il messaggio di errore seguente: {}.&#8233;&#8233;Traceback:&#8233;&#8233;{}&#8233;Riprova?</translation></message>
</context>
<context>
    <name>self.qd</name>
    <message>
        <location filename="../ui/editor.py" line="55" />
        <source>Please enter your password:</source>
        <translation>Per favore inserisci la tua password:</translation></message>
    <message>
        <location filename="../ui/editor.py" line="63" />
        <source>Token for {} not found. Have you added this server to your settings?.</source>
        <translation>Token per {} non trovato. Hai aggiunto questo server alle tue impostazioni?</translation></message>
    <message>
        <location filename="../ui/editor.py" line="73" />
        <source>Error while creating branch {}.</source>
        <translation>Errore nel creare il branch {}.</translation></message>
</context>
</TS>