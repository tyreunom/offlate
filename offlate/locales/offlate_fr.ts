<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS language="fr"><context>
    <name>AboutWindow</name>
    <message>
        <location filename="../ui/about.py" line="45" />
        <source>Offlate is a translation interface for offline translation of projects using online platforms. Offlate is free software, you can redistribute it under the GPL v3 license or any later version.</source>
        <translation>Offlate est une interface de traduction hors-ligne de projets qui utilisent des plateformes en ligne. Offlate est un logiciel libre, vous pouvez le redistribuer sous la licence GPL v3 ou toute version ult&#233;rieure.</translation></message>
    <message>
        <location filename="../ui/about.py" line="53" />
        <source>Report an issue</source>
        <translation>Rapporter un probl&#232;me</translation></message>
    <message>
        <location filename="../ui/about.py" line="54" />
        <source>Close this window</source>
        <translation>Fermer cette fen&#234;tre</translation></message>
    <message>
        <location filename="../ui/about.py" line="49" />
        <source>Copyright (C) 2018, 2019 Julien Lepiller</source>
        <translation type="obsolete">Copyright &#169; 2018, 2019 Julien Lepiller
Pour la traduction francophone :
Copyright &#169; 2018, 2019 Julien Lepiller</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="51" />
        <source>Copyright (C) 2018-2021 Julien Lepiller</source>
        <translation>Copyright (C) 2018-2021 Julien Lepiller</translation></message>
</context>
<context>
    <name>AdvancedProjectWidget</name>
    <message>
        <location filename="../ui/new.py" line="94" />
        <source>Project information</source>
        <translation type="obsolete">Informations sur le projet</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="102" />
        <source>Name:</source>
        <translation type="obsolete">Nom :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="103" />
        <source>Target Language:</source>
        <translation type="obsolete">Langue cible :</translation>
    </message>
</context>
<context>
    <name>CopyrightSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="139" />
        <source>In some cases, we need to add a copyright line for you in the translation files we send upstream. Here, you can configure how you want the copyright line to look like. This information will most likely become public after your first contribution.</source>
        <translation>Dans certains cas, nous avons besoin d'ajouter une ligne concernant vos droits d'auteur dans les fichiers de traduction que nous envoyons en amont. Ici, vous pouvez configurer la mani&#232;re dont vous voulez que cette ligne d'information apparaisse. Ces informations seront probablement publiques apr&#232;s votre premi&#232;re contribution.</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="186" />
        <source>John Doe</source>
        <translation>Jean Dupont</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="188" />
        <source>john@doe.me</source>
        <translation>jean@dupont.me</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="160" />
        <source>Here is how your copyright line will look like:</source>
        <translation>Voici ce &#224; quoi votre ligne d'information ressemblera :</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="189" />
        <source>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</source>
        <translation>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</translation></message>
</context>
<context>
    <name>EditorWindow</name>
    <message>
        <location filename="../ui/editor.py" line="551" />
        <source>Unsupported / Unknown project</source>
        <translation>Projet inconnu ou non support&#233;</translation></message>
    <message numerus="yes">
        <location filename="../ui/editor.py" line="573" />
        <source>{} translated on {} total ({}%).</source>
        <translation><numerusform>{} traduit sur {} au total ({} %).</numerusform><numerusform>{} traduits sur {} au total ({} %).</numerusform></translation></message>
    <message>
        <location filename="../ui/editor.py" line="672" />
        <source>Exit</source>
        <translation>Quitter</translation></message>
    <message>
        <location filename="../ui/editor.py" line="674" />
        <source>Exit application</source>
        <translation>Quitter l'application</translation></message>
    <message>
        <location filename="../ui/editor.py" line="677" />
        <source>Save</source>
        <translation>Sauvegarder</translation></message>
    <message>
        <location filename="../ui/editor.py" line="679" />
        <source>Save current project</source>
        <translation>Sauvegarder le projet actuel</translation></message>
    <message>
        <location filename="../ui/editor.py" line="682" />
        <source>New</source>
        <translation>Nouveau</translation></message>
    <message>
        <location filename="../ui/editor.py" line="684" />
        <source>New project</source>
        <translation>Nouveau projet</translation></message>
    <message>
        <location filename="../ui/editor.py" line="687" />
        <source>Manage Projects</source>
        <translation>G&#233;rer les projets</translation></message>
    <message>
        <location filename="../ui/editor.py" line="690" />
        <source>Open project manager</source>
        <translation>Ouvrir le gestionnaire de projets</translation></message>
    <message>
        <location filename="../ui/editor.py" line="693" />
        <source>Update</source>
        <translation>Mettre &#224; jour</translation></message>
    <message>
        <location filename="../ui/editor.py" line="695" />
        <source>Get modifications from upstream</source>
        <translation>R&#233;cup&#233;rer les modifications en amont</translation></message>
    <message>
        <location filename="../ui/editor.py" line="698" />
        <source>Close</source>
        <translation>Fermer</translation></message>
    <message>
        <location filename="../ui/editor.py" line="699" />
        <source>Close current project</source>
        <translation>Fermer le projet actuel</translation></message>
    <message>
        <location filename="../ui/editor.py" line="702" />
        <source>Send</source>
        <translation>Envoyer</translation></message>
    <message>
        <location filename="../ui/editor.py" line="704" />
        <source>Send modifications upstream</source>
        <translation>Envoyer les modifications en amont</translation></message>
    <message>
        <location filename="../ui/editor.py" line="707" />
        <source>Settings</source>
        <translation>Param&#232;tres</translation></message>
    <message>
        <location filename="../ui/editor.py" line="709" />
        <source>Set parameters</source>
        <translation>Configurer les param&#232;tres</translation></message>
    <message>
        <location filename="../ui/editor.py" line="722" />
        <source>Show Translated</source>
        <translation>Montrer les cha&#238;nes traduites</translation></message>
    <message>
        <location filename="../ui/editor.py" line="725" />
        <source>Show Fuzzy</source>
        <translation>Montrer les traductions floues</translation></message>
    <message>
        <location filename="../ui/editor.py" line="728" />
        <source>Show Empty Translation</source>
        <translation>Montrer les traductions vides</translation></message>
    <message>
        <location filename="../ui/editor.py" line="731" />
        <source>Use a monospace font</source>
        <translation>Utiliser une police &#224; chasse fixe</translation></message>
    <message>
        <location filename="../ui/editor.py" line="756" />
        <source>Open</source>
        <translation>Ouvrir</translation></message>
    <message>
        <location filename="../ui/editor.py" line="760" />
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation></message>
    <message>
        <location filename="../ui/editor.py" line="768" />
        <source>&amp;Project</source>
        <translation>&amp;Projet</translation></message>
    <message>
        <location filename="../ui/editor.py" line="775" />
        <source>&amp;Edit</source>
        <translation>&amp;&#201;dition</translation></message>
    <message>
        <location filename="../ui/editor.py" line="780" />
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation></message>
    <message>
        <location filename="../ui/editor.py" line="585" />
        <source>Uploading {}...</source>
        <translation>Envoi de {}&#8230;</translation></message>
    <message>
        <location filename="../ui/editor.py" line="592" />
        <source>Finished uploading {}!</source>
        <translation>Fin de l'envoi de {} !</translation></message>
    <message>
        <location filename="../ui/editor.py" line="595" />
        <source>Updating {}...</source>
        <translation>Mise &#224; jour de {}&#8230;</translation></message>
    <message>
        <location filename="../ui/editor.py" line="607" />
        <source>Finished updating {}!</source>
        <translation>Fin de la mise &#224; jour de {} !</translation></message>
    <message>
        <location filename="../ui/editor.py" line="712" />
        <source>Search</source>
        <translation>Rechercher</translation></message>
    <message>
        <location filename="../ui/editor.py" line="714" />
        <source>Search in the document</source>
        <translation>Rechercher dans le document</translation></message>
    <message>
        <location filename="../ui/editor.py" line="717" />
        <source>Replace</source>
        <translation>Remplacer</translation></message>
    <message>
        <location filename="../ui/editor.py" line="719" />
        <source>Replace content in the document</source>
        <translation>Remplacer le contenu dans le document</translation></message>
</context>
<context>
    <name>GitlabEdit</name>
    <message>
        <location filename="../ui/gitlabedit.py" line="59" />
        <source>server</source>
        <translation>serveur</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="61" />
        <source>token</source>
        <translation>jeton</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="62" />
        <source>Add</source>
        <translation>Ajouter</translation></message>
    <message>
        <location filename="../ui/gitlabedit.py" line="64" />
        <source>Remove</source>
        <translation>Supprimer</translation></message>
</context>
<context>
    <name>LangSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="204" />
        <source>Which language will you most frequently translate projects into? This setting can be overriden in each individual projects.</source>
        <translation>Vers quelle langue traduirez-vous vos projets le plus fr&#233;quemment? Ce param&#232;tre peut &#234;tre chang&#233; individuellement dans chaque projet.</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="211" />
        <source>None</source>
        <translation>Aucune</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="229" />
        <source>Note that you should only translate into native language or to a language you are extremely familiar with, to avoid weird or nonsensical translations.</source>
        <translation type="obsolete">Remarquez que vous ne devriez traduire que vers votre langue natale ou vers une langue que vous connaissez tr&#232;s bien, pour &#233;viter les traduction maladroites ou les contre-sens.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="229" />
        <source>Note that you should only translate into your native language or to a language you are extremely familiar with, to avoid weird or nonsensical translations.</source>
        <translation>Remarquez que vous ne devriez traduire que vers votre langue natale ou vers une langue avec laquelle vous &#234;tes extr&#234;mement bien &#224; l'aise, pour &#233;viter les traductions &#233;tranges ou qui n'ont aucun sens.</translation></message>
</context>
<context>
    <name>ListSettingsEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="142" />
        <source>Add</source>
        <translation>Ajouter</translation></message>
    <message>
        <location filename="../ui/settingsedit.py" line="144" />
        <source>Remove</source>
        <translation>Supprimer</translation></message>
</context>
<context>
    <name>ListSettingsRowEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="53" />
        <source>OK</source>
        <translation>OK</translation></message>
</context>
<context>
    <name>MultipleLineEdit</name>
    <message>
        <location filename="../ui/multiplelineedit.py" line="59" />
        <source>Add</source>
        <translation>Ajouter</translation></message>
    <message>
        <location filename="../ui/multiplelineedit.py" line="61" />
        <source>Remove</source>
        <translation>Supprimer</translation></message>
</context>
<context>
    <name>NewWindow</name>
    <message>
        <location filename="../ui/new.py" line="68" />
        <source>The Translation Project</source>
        <translation type="obsolete">Le Projet de Traduction</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="230" />
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="231" />
        <source>OK</source>
        <translation>OK</translation></message>
    <message>
        <location filename="../ui/new.py" line="57" />
        <source>Project information</source>
        <translation type="obsolete">Informations sur le projet</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="65" />
        <source>Name:</source>
        <translation type="obsolete">Nom :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="66" />
        <source>Target Language:</source>
        <translation type="obsolete">Langue cible :</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="69" />
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="100" />
        <source>Organization</source>
        <translation type="obsolete">Organisation</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="70" />
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="122" />
        <source>repository</source>
        <translation type="obsolete">d&#233;p&#244;t</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="127" />
        <source>branch</source>
        <translation type="obsolete">branche</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="71" />
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
</context>
<context>
    <name>PageCopyright</name>
    <message>
        <location filename="../ui/welcome.py" line="98" />
        <source>Copyright Settings</source>
        <translation>Param&#232;tres des droits d'auteur</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="99" />
        <source>Configuring how your copyright is added to files</source>
        <translation>Configuration de la mani&#232;re d'ajouter vos droits d'auteur aux fichiers</translation></message>
</context>
<context>
    <name>PageDownload</name>
    <message>
        <location filename="../ui/welcome.py" line="188" />
        <source>Project Fetch</source>
        <translation>R&#233;cup&#233;ration du projet</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="189" />
        <source>Downloading translation files</source>
        <translation>T&#233;l&#233;chargement des fichiers de configuration</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="192" />
        <source>We are now attempting to fetch the translation files of your project. This step should be pretty fast.</source>
        <translation>Nous essayons maintenant de r&#233;cup&#233;rer les fichiers de traduction de votre projet. Cette &#233;tape devrait &#234;tre assez rapide.</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="222" />
        <source>Finished!</source>
        <translation>Termin&#233; !</translation></message>
</context>
<context>
    <name>PageHints</name>
    <message>
        <location filename="../ui/welcome.py" line="302" />
        <source>You can always go back to the initial screen (called the project manager) from the editor, by using &lt;i&gt;file &gt; project manager&lt;/i&gt;</source>
        <translation>Vous pouvez toujours revenir &#224; l'&#233;cran d'accueil (appel&#233; le gestionnaire de projets) depuis l'&#233;diteur, en utilisant &lt;i&gt; fichier &gt; gestionnaire de projets&lt;/i&gt;</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="311" />
        <source>Quickly switch to the next string with &lt;i&gt;Ctrl+Enter&lt;/i&gt;. Use &lt;i&gt;Ctrl+Shift+Enter&lt;/i&gt; to go back to the previous string instead.</source>
        <translation>Passez rapidement &#224; la chaine suivante avec &lt;i&gt;Ctrl+Entr&#233;e&lt;/i&gt;. Utilisez &lt;i&gt;Ctrl+Shift+Entr&#233;e&lt;/i&gt; pour revenir &#224; la chaine pr&#233;c&#233;dente.</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="320" />
        <source>With these hints, you are now ready to start working on your project! We will now open the project manager, from which you can add more projects, change your settings, etc. Have fun!</source>
        <translation>Avec ces astuces, vous voil&#224; maintenant pr&#234;t&#183;e &#224; travailler sur votre projet ! Nous allons maintenant ouvrir le gestionnaire de projets, &#224; partir duquel vous pouvez ajouter plus de projets, changer vos param&#232;tres, etc. Amusez-vous bien !</translation></message>
</context>
<context>
    <name>PageLang</name>
    <message>
        <location filename="../ui/welcome.py" line="117" />
        <source>Language Settings</source>
        <translation>Param&#232;tre linguistiques</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="118" />
        <source>Configuring the default language for new projects</source>
        <translation>Configuration de la langue par d&#233;faut des nouveaux projets</translation></message>
</context>
<context>
    <name>PageProjectSelection</name>
    <message>
        <location filename="../ui/welcome.py" line="140" />
        <source>First Project</source>
        <translation>Premier projet</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="141" />
        <source>Choosing a first project</source>
        <translation>Choix d'un premier projet</translation></message>
</context>
<context>
    <name>PageSuccess</name>
    <message>
        <location filename="../ui/welcome.py" line="279" />
        <source>Congratulations! We've just set up your first project! Initial setup is now complete, and you can always change the settings you've set here from the Offlate welcome screen. After a few explanations you will be able to work on your project right away!</source>
        <translation>F&#233;licitations ! Nous venons tout juste de configurer votre premier projet ! La configuration initiale est maintenant termin&#233;e et vous pouvez toujours changer les param&#232;tres que vous venez d'initialiser ici &#224; partir de l'&#233;cran d'accueil d'Offlate. Apr&#232;s quelques explications vous pourrez travailler sur votre projet imm&#233;diatement !</translation></message>
</context>
<context>
    <name>PageSystemSettings</name>
    <message>
        <location filename="../ui/welcome.py" line="163" />
        <source>System Configuration</source>
        <translation>Configuration du syst&#232;me</translation></message>
    <message>
        <location filename="../ui/welcome.py" line="164" />
        <source>Getting an account on the project's platform</source>
        <translation>Cr&#233;ation d'un compte sur la plateforme du projet</translation></message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../ui/welcome.py" line="81" />
        <source>Offlate is a tool to help you localise free and open source software. Before you start contributing translations to a project though, there are a few things we need to set up and talk about. Let's get started!</source>
        <translation>Offlate est un outil qui vous aide &#224; traduire les logiciels libres et ouverts. Avant de commencer &#224; contribuer des traductions pour un projet, il y a cependant quelques petites choses que nous devons param&#233;trer et dont nous devons parler. C'est parti !</translation></message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../systems/gitlab.py" line="92" />
        <source>Repository</source>
        <translation>D&#233;p&#244;t</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="92" />
        <source>Full clone URL for the repository</source>
        <translation>URL compl&#232;te pour le clonage du d&#233;p&#244;t</translation></message>
    <message>
        <location filename="../systems/github.py" line="89" />
        <source>https://...</source>
        <translation>https://&#8230;</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>Branch</source>
        <translation>Branche</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>Name of the branch to translate</source>
        <translation>Nom de la branche &#224; traduire</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="95" />
        <source>master</source>
        <translation>master</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Token</source>
        <translation>Jeton</translation></message>
    <message>
        <location filename="../systems/github.py" line="98" />
        <source>You can get a token from &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;. You will need at least to grant the public_repo permission.</source>
        <translation>Vous pouvez r&#233;cup&#233;rer un jeton sur &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;. Vous devrez au moins donner la permission public_repo.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>Gitlab instance configuration</source>
        <translation>Configuration de l'instance Gitlab</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>You need to configure each Gitlab instance separately, and you haven't configured the instance at {} yet.</source>
        <translation>Vous devez configurer chaque instance Gitlab s&#233;par&#233;ment, et vous n'avez pas encore configur&#233; l'instance {}.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="105" />
        <source>The token you created from your account. You can create it from &lt;a href="#"&gt;the Access Tokens tab&lt;/a&gt; in your account settings.</source>
        <translation>Le jeton que vous avez cr&#233;&#233; depuis votre compte. Vous pouvez le cr&#233;er &#224; partir de &lt;a href="#"&gt;l'onglet Access Tokens&lt;/a&gt; de vos param&#232;tres de compte.</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="119" />
        <source>Configured Gitlab instances</source>
        <translation>Instances Gitlab configur&#233;es</translation></message>
    <message>
        <location filename="../systems/gitlab.py" line="119" />
        <source>You need to create a token for each Gitlab instance you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation>Vous devez cr&#233;er un jeton pour chaque instance Gitlab sur lesquelles vous poss&#233;dez un compte. Vous pouvez cr&#233;er un jeton en vous connectant &#224; votre compte, en allant dans vos param&#232;tres et sur la page Access Tokens.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Server</source>
        <translation>Serveur</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Server name</source>
        <translation>Nom du serveur</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>The token you created from your account</source>
        <translation>Le jeton que vous avez cr&#233;&#233; pour votre compte</translation></message>
    <message>
        <location filename="../systems/tp.py" line="153" />
        <source>version</source>
        <translation>version</translation></message>
    <message>
        <location filename="../systems/tp.py" line="153" />
        <source>version of the project (keep empty for latest)</source>
        <translation>version du projet (laissez vide pour la derni&#232;re version)</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>Mail server</source>
        <translation>Serveur de courriel</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>To send your work to the translation project on your behalf, we need to know the email server you are going to use (usually the part on the right of the `@` in your email address).</source>
        <translation>Pour envoyer votre travail au projet de traduction &#224; votre place, nous devons connaitre le serveur de courriel que vous utiliser (habituellement la partie &#224; droite du `@' dans votre adresse de courriel).</translation></message>
    <message>
        <location filename="../systems/tp.py" line="159" />
        <source>example.com</source>
        <translation>example.com</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>Mail user</source>
        <translation>Utilisateur</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>Username used to connect to your mail server, usually the email address itself, or the part of the left of the `@`.</source>
        <translation>Le nom d'utilisateur utilis&#233; pour vous connecter &#224; votre serveur de courriel, habituellement l'adresse de courriel enti&#232;re ou la partie &#224; gauche du `@'.</translation></message>
    <message>
        <location filename="../systems/tp.py" line="164" />
        <source>john</source>
        <translation>jean</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="147" />
        <source>You can get a token from &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete">Vous pouvez r&#233;cup&#233;rer un jeton sur &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153" />
        <source>Organization</source>
        <translation>Organisation</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="153" />
        <source>The organization this project belongs in, on transifex.</source>
        <translation>L'organisation &#224; laquelle ce projet appartient, sur transifex.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>Project</source>
        <translation>Projet</translation></message>
    <message>
        <location filename="../systems/transifex.py" line="155" />
        <source>The name of the project on transifex</source>
        <translation>Le nom du projet sur transifex</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>Instance</source>
        <translation>Instance</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="215" />
        <source>Full clone URL of the Weblate instance</source>
        <translation type="obsolete">URL de l'instance Weblate</translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>https://weblate.org</source>
        <translation>https://hosted.weblate.org</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>Name of the project on the instance</source>
        <translation>Nom du projet sur l'instance</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="285" />
        <source>foo</source>
        <translation>toto</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>Weblate instance configuration</source>
        <translation>Configuration de l'instance Weblate</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>You need to configure each Weblate instance separately, and you haven't configured the instance at {} yet.</source>
        <translation>Vous devez configurer chaque instance Weblate s&#233;par&#233;ment, et vous n'avez par encore configur&#233; l'instance {}.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="292" />
        <source>The token you created from your account. You can create it from &lt;a href="#"&gt;the API access tab&lt;/a&gt; in your account settings.</source>
        <translation>Le jeton que vous avez cr&#233;&#233; depuis votre compte. Vous pouvez le cr&#233;er &#224; partir de &lt;a href="#"&gt;l'onglet API access&lt;/a&gt; dans vos param&#232;tres de compte.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</source>
        <translation>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>Configured Weblate instances</source>
        <translation>Instances Weblate configur&#233;es</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="303" />
        <source>You need to find a token for each Weblate instance you have an account on. You can create a token by logging into your account, going to your settings and in the API Access page.</source>
        <translation>Vous devez trouver un jeton pour chaque instance Weblate sur lesquelles vous avez un compte. Vous pouvez cr&#233;er un jeton en vous connectant &#224; votre compte, en allant dans vos param&#232;tres et sur la page API Access.</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>URL of the Weblate instance</source>
        <translation>URL de l'instance Weblate</translation></message>
    <message>
        <location filename="../systems/weblate.py" line="282" />
        <source>https://hosted.weblate.org</source>
        <translation>https://hosted.weblate.org</translation></message>
</context>
<context>
    <name>ProjectManagerWidget</name>
    <message>
        <location filename="../ui/manager.py" line="89" />
        <source>Open</source>
        <translation>Ouvrir</translation></message>
    <message>
        <location filename="../ui/manager.py" line="90" />
        <source>Edit</source>
        <translation>Modifier</translation></message>
    <message>
        <location filename="../ui/manager.py" line="91" />
        <source>Remove</source>
        <translation>Supprimer</translation></message>
    <message>
        <location filename="../ui/manager.py" line="99" />
        <source>New Project</source>
        <translation>Nouveau projet</translation></message>
    <message>
        <location filename="../ui/manager.py" line="100" />
        <source>Settings</source>
        <translation>Param&#232;tres</translation></message>
    <message>
        <location filename="../ui/manager.py" line="101" />
        <source>About Offlate</source>
        <translation>&#192; propos d'Offlate</translation></message>
    <message>
        <location filename="../ui/manager.py" line="102" />
        <source>Exit</source>
        <translation>Quitter</translation></message>
    <message>
        <location filename="../ui/manager.py" line="219" />
        <source>Fetching project {}...</source>
        <translation>R&#233;cup&#233;ration du projet {}&#8230;</translation></message>
</context>
<context>
    <name>ProjectManagerWindow</name>
    <message>
        <location filename="../ui/manager.py" line="53" />
        <source>Offlate Project Manager</source>
        <translation>Gestionnaire de projets d'Offlate</translation></message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <location filename="../ui/editor.py" line="303" />
        <source>Singular</source>
        <translation>Singulier</translation></message>
    <message>
        <location filename="../ui/editor.py" line="304" />
        <source>Plural</source>
        <translation>Pluriel</translation></message>
    <message>
        <location filename="../ui/editor.py" line="159" />
        <source>Copy</source>
        <translation>Copier</translation></message>
    <message>
        <location filename="../ui/editor.py" line="155" />
        <source>Open in external editor</source>
        <translation>Ouvrir dans un &#233;diteur externe</translation></message>
    <message>
        <location filename="../ui/editor.py" line="368" />
        <source>&lt;b&gt;location&lt;/b&gt;: {0} line {1}</source>
        <translation>&lt;b&gt;emplacement&lt;/b&gt; : {0} ligne {1}</translation></message>
    <message>
        <location filename="../ui/editor.py" line="370" />
        <source>&lt;b&gt;comment&lt;/b&gt;: {0}</source>
        <translation>&lt;b&gt;commentaire&lt;/b&gt; : {0}</translation></message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="../ui/search.py" line="84" />
        <source>String to search for</source>
        <translation>Chaine &#224; rechercher</translation></message>
    <message>
        <location filename="../ui/search.py" line="87" />
        <source>String to replace into</source>
        <translation>Chaine &#224; utiliser &#224; la place</translation></message>
    <message>
        <location filename="../ui/search.py" line="92" />
        <source>Search</source>
        <translation>Rechercher</translation></message>
    <message>
        <location filename="../ui/search.py" line="93" />
        <source>Replace</source>
        <translation>Remplacer</translation></message>
    <message>
        <location filename="../ui/search.py" line="102" />
        <source>Case sensitive</source>
        <translation>Sensible &#224; la casse</translation></message>
    <message>
        <location filename="../ui/search.py" line="103" />
        <source>Wrap around</source>
        <translation>Faire le tour</translation></message>
    <message>
        <location filename="../ui/search.py" line="105" />
        <source>Match whole word only</source>
        <translation>Trouver seulement des mots complets</translation></message>
    <message>
        <location filename="../ui/search.py" line="106" />
        <source>Search in original text</source>
        <translation>Rechercher dans le texte d'origine</translation></message>
    <message>
        <location filename="../ui/search.py" line="108" />
        <source>Search in translated text</source>
        <translation>Rechercher dans la traduction</translation></message>
    <message>
        <location filename="../ui/search.py" line="110" />
        <source>Search in comments</source>
        <translation>Rechercher dans les commentaires</translation></message>
    <message>
        <location filename="../ui/search.py" line="122" />
        <source>Close</source>
        <translation>Fermer</translation></message>
    <message>
        <location filename="../ui/search.py" line="126" />
        <source>Replace all</source>
        <translation>Tout remplacer</translation></message>
    <message>
        <location filename="../ui/search.py" line="128" />
        <source>Replace one</source>
        <translation>Remplacer une fois</translation></message>
    <message>
        <location filename="../ui/search.py" line="130" />
        <source>&lt; Previous</source>
        <translation>&lt; Pr&#233;c&#233;dent</translation></message>
    <message>
        <location filename="../ui/search.py" line="132" />
        <source>Next &gt;</source>
        <translation>Suivant &gt;</translation></message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="287" />
        <source>Configure me</source>
        <translation>Configurez-moi</translation></message>
    <message>
        <location filename="../ui/config/settings.py" line="308" />
        <source>Done!</source>
        <translation>Termin&#233; !</translation></message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../ui/settings.py" line="42" />
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="43" />
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="129" />
        <source>Email:</source>
        <translation type="obsolete">Adresse de courriel :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="130" />
        <source>Server:</source>
        <translation type="obsolete">Serveur :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="131" />
        <source>User Name:</source>
        <translation type="obsolete">Nom d'utilisateur :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="56" />
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="68" />
        <source>You can get a token from &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete">Vous pouvez r&#233;cup&#233;rer un jeton sur &lt;a href="#"&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="164" />
        <source>Token:</source>
        <translation type="obsolete">Jeton :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="108" />
        <source>Translation Project</source>
        <translation type="obsolete">Projet de traduction</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="178" />
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="180" />
        <source>Add your gitlab account tokens below. You need to create a token for every gitlab server you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation type="obsolete">Ajoutez les jetons de vos comptes gitlab ci-dessous. Vous devrez cr&#233;er un jeton par serveur gitlab sur lequel vous avez un compte. Vous pouvez cr&#233;er un jeton en vous connectant &#224; votre compte, en allant dans les param&#232;tres et sur la page jetons d'acc&#232;s.</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="86" />
        <source>Generic Settings</source>
        <translation type="obsolete">Param&#232;tres g&#233;n&#233;raux</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="89" />
        <source>John Doe &lt;john@doe.me&gt;</source>
        <translation type="obsolete">Jean Dupont &lt;jean@dupont.fr&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="96" />
        <source>Full Name:</source>
        <translation type="obsolete">Nom complet :</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="101" />
        <source>Generic</source>
        <translation type="obsolete">G&#233;n&#233;ral</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="147" />
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="159" />
        <source>You can get a token from &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;.             You will need at least to grant the public_repo permission.</source>
        <translation type="obsolete">Vous pouvez r&#233;cup&#233;rer un jeton sur &lt;a href="#"&gt;https://github.com/settings/tokens/new&lt;/a&gt;.
Vous aurez besoin d'au moins la permission public_repo.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="42" />
        <source>Offlate Settings</source>
        <translation>Param&#232;tres d'Offlate</translation></message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="../ui/spellcheckedit.py" line="47" />
        <source>Spelling Suggestions</source>
        <translation>Suggestions</translation></message>
    <message>
        <location filename="../ui/spellcheckedit.py" line="48" />
        <source>No Suggestions</source>
        <translation>Pas de suggestion</translation></message>
</context>
<context>
    <name>SystemSettingsWindow</name>
    <message>
        <location filename="../ui/config/settings.py" line="116" />
        <source>OK</source>
        <translation>OK</translation></message>
</context>
<context>
    <name>WelcomeWindow</name>
    <message>
        <location filename="../ui/config/welcome.py" line="38" />
        <source>Welcome to Offlate</source>
        <translation>Bienvenue dans Offlate</translation></message>
</context>
<context>
    <name>dialog</name>
    <message>
        <location filename="../ui/main.py" line="60" />
        <source>The app just crashed, please report the following error, with any relevant information (what you did when the app crashed, any external factor that might be relevant, etc.). You can send your report on {}, or by email to {}.

{}
Traceback:
{}</source>
        <translation>L'appli vient de crasher, veuillez rapporter l'erreur suivante avec toutes les informations utiles (ce que vous faisiez au moment du plantage, les facteurs externes qui pourraient avoir un rapport, etc). Vous pouvez &#233;crire votre rapport sur {} ou l'envoyer par courriel &#224; {}.&#8233;&#8233;{}&#8233;Traceback:&#8233;{}</translation></message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="../ui/parallel.py" line="57" />
        <source>A project with the same name already exists. The new project was not created. You should first remove the same-named project.</source>
        <translation>Il existe d&#233;j&#224; un projet avec le m&#234;me nom. Le nouveau projet n'a pas &#233;t&#233; cr&#233;&#233;. Vous devriez supprimer le projet qui a le m&#234;me nom.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="61" />
        <source>Your filesystem contains a same-named directory for your new project. The new project was not created. You should first remove the same-named directory: "{}".</source>
        <translation>Votre syst&#232;me de fichiers contient d&#233;j&#224; un r&#233;pertoire avec le m&#234;me nom que votre nouveau projet. Le nouveau projet n'a pas &#233;t&#233; cr&#233;&#233;. Vous devriez d'abord supprimer le r&#233;pertoire avec le m&#234;me nom : &#171; {} &#187;.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="67" />
        <source>The project you added uses the {} format, but it is not supported yet by Offlate. You can try to update the application, or if you are on the latest version already, report it as a bug.</source>
        <translation>Le projet que vous avez ajout&#233; utilise le format {}, mais il n'est pas encore pris en charge par Offlate. Vous pouvez essayer de mettre &#224; jour l'application, ou si vous &#234;tes d&#233;j&#224; sur la derni&#232;re version, rapportez un bogue.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="75" />
        <source>An unexpected error occured while fetching the project: {}. You should report this as a bug.</source>
        <translation>Une erreur inattendue a eu lieue lors de la r&#233;cup&#233;ration du projet : {}. Vous devriez rapporter un bogue.</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="71" />
        <source>The project {} you added could not be found in the translation platform you selected. Did you make a typo while entering the name or other parameters?</source>
        <translation>Le projet {} que vous avez ajout&#233; n'a pas &#233;t&#233; trouv&#233; sur la plateforme de traduction de vous avez choisie. Avez-vous fait une coquille en tapant son nom ou d'autres param&#232;tres ?</translation></message>
    <message>
        <location filename="../ui/parallel.py" line="48" />
        <source>This action did not complete correctly. Try again, and if the issue persists, consider sending a bug report at {}, or by email to {} with the following information, and any relevant information (what you were trying to do, other external factors, etc.) We received the following error message: {}.

Traceback:

{}
Try again?</source>
        <translation>Cette action ne s'est pas termin&#233;e correctement. Essayez de nouveau et si l'erreur persiste, envoyez un rapport de bug sur {} ou par courriel &#224; {} avec les informations suivante et toute information pertinent (ce que vous essayiez de faire, d'autres facteurs externes, etc). Nous avons re&#231;u le message d'erreur suivant : {}.&#8233;&#8233;Traceback:&#8233;&#8233;{}&#8233;Essayez de nouveau ?</translation></message>
</context>
<context>
    <name>self.qd</name>
    <message>
        <location filename="../ui/editor.py" line="55" />
        <source>Please enter your password:</source>
        <translation>Saisissez votre mot de passe :</translation></message>
    <message>
        <location filename="../ui/editor.py" line="63" />
        <source>Token for {} not found. Have you added this server to your settings?.</source>
        <translation>Jeton pour {} introuvable. Avez-vous ajout&#233; ce serveur &#224; vos param&#232;tres ?</translation></message>
    <message>
        <location filename="../ui/editor.py" line="73" />
        <source>Error while creating branch {}.</source>
        <translation>Erreur lors de la cr&#233;ation de la branche {}.</translation></message>
</context>
</TS>