#   Copyright (c) 2019, 2021 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

import json
import os
import re
import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ..core.config import *
from ..systems.list import *

class PredefinedProjectWidget(QWidget):
    currentItemChanged = pyqtSignal()

    def __init__(self, parent = None):
        super().__init__(parent)
        self.setProperty("data", {})
        self.initUI()

    def initUI(self):
        predefinedbox = QVBoxLayout()
        self.searchfield = QLineEdit()
        self.searchfield.addAction(QIcon.fromTheme("system-search"), QLineEdit.LeadingPosition)
        predefinedbox.addWidget(self.searchfield)
        self.predefinedprojects = QListWidget()
        with open(os.path.dirname(__file__) + '/../data/data.json') as f:
            self.projectdata = json.load(f)
            for d in self.projectdata:
                item = QListWidgetItem(d['name'])
                item.setData(Qt.UserRole, d)
                self.predefinedprojects.addItem(item)
        predefinedbox.addWidget(self.predefinedprojects)

        self.predefinedprojects.currentItemChanged.connect(self.itemChanged)

        self.searchfield.textChanged.connect(self.filter)
        self.setLayout(predefinedbox)

    def itemChanged(self):
        item = self.currentItem()
        if item is None:
            self.setProperty("data", {})
        self.setProperty("data", item.data(Qt.UserRole))
        self.currentItemChanged.emit()

    def currentItem(self):
        return self.predefinedprojects.currentItem()

    def filter(self):
        search = self.searchfield.text()
        self.predefinedprojects.clear()
        regexp = re.compile(".*"+search)
        for d in self.projectdata:
            if regexp.match(d['name']):
                item = QListWidgetItem(d['name'])
                item.setData(Qt.UserRole, d)
                self.predefinedprojects.addItem(item)

    def currentData(self):
        return self.property("data")

class AdvancedProjectWidget(QWidget):
    modified = pyqtSignal()

    def __init__(self, parent = None, name = "", lang = "", system = 0, info = None, followup = []):
        super().__init__(parent)
        self.name = name
        self.lang = lang
        self.system = system
        self.info = info
        self.followupWidgets = followup
        self._registerAdditionalFields()
        self.initUI()

    def initUI(self):
        contentbox = QVBoxLayout()
        formbox = QGroupBox(self.tr("Project information"))
        self.formLayout = QFormLayout()
        formbox.setLayout(self.formLayout)

        self.nameWidget = QLineEdit()
        self.nameWidget.setText(self.name)
        self.langWidget = QLineEdit()
        self.langWidget.setText(self.lang)
        self.formLayout.addRow(QLabel(self.tr("Name:")), self.nameWidget)
        self.formLayout.addRow(QLabel(self.tr("Target Language:")), self.langWidget)
        self.combo = QComboBox()
        for system in systems:
            self.combo.addItem(self.tr(system['name']))
        self.combo.setCurrentIndex(self.system)
        self.formLayout.addRow(self.combo)

        self.nameWidget.textChanged.connect(self.modify)
        self.langWidget.textChanged.connect(self.modify)

        contentbox.addWidget(formbox)
        self.combo.currentIndexChanged.connect(self.othersystem)

        self.setLayout(contentbox)

        self.othersystem(init = True)

    def fill(self, data):
        self.nameWidget.setText(data['name'])
        self.combo.setCurrentIndex(int(data['system']))

        system = systems[data['system']]
        fields = self.additionalFields[data['system']]
        i = 0
        for spec in system['system'].getProjectConfigSpec():
            widget = fields[i]['widget']
            if spec.key in data:
                widget.setText(data[spec.key])
            i = i + 1
        self.modified.emit()

    def isComplete(self):
        complete = False
        if self.nameWidget.text() != '' and self.langWidget.text() != '':
            complete = True
            system = systems[self.combo.currentIndex()]
            fields = self.additionalFields[self.combo.currentIndex()]
            i = 0
            for spec in system['system'].getProjectConfigSpec():
                if not spec.optional:
                    widget = fields[i]['widget']
                    if isinstance(widget, QLineEdit) and widget.text() == '':
                        complete = False
                        break
                i = i + 1
        return complete

    def _registerAdditionalFields(self):
        self.additionalFields = []

        for system in systems:
            fields = []
            for spec in system['system'].getProjectConfigSpec():
                if isinstance(spec, StringConfigSpec):
                    widget = QLineEdit()
                    widget.textChanged.connect(self.modify)
                    if spec.placeholder is not None and spec.placeholder != '':
                        widget.setPlaceholderText(spec.placeholder)
                    label = QLabel(spec.name)
                    fields.append({'label': label, 'widget': widget, 'key': spec.key})
                else:
                    raise Exception("Unknown spec type: " + spec)
            self.additionalFields.append(fields)

    def getProjectName(self):
        return self.nameWidget.text()

    def getProjectLang(self):
        return self.langWidget.text()

    def getProjectSystem(self):
        return self.combo.currentIndex()

    def getProjectInfo(self):
        ans = {}
        system = systems[self.getProjectSystem()]
        fields = self.additionalFields[self.getProjectSystem()]
        i = 0
        for spec in system['system'].getProjectConfigSpec():
            ans[spec.key] = fields[i]['widget'].text()
            i = i + 1
        return ans

    def othersystem(self, init = False):
        for system in self.additionalFields:
            for widget in system:
                self.formLayout.takeRow(widget['widget'])
                widget['widget'].hide()
                widget['label'].hide()
        self.formLayout.invalidate()
        oldwidget = self.combo
        for widget in self.additionalFields[self.combo.currentIndex()]:
            self.setTabOrder(oldwidget, widget['widget'])
            oldwidget = widget['widget']
            self.formLayout.addRow(widget['label'], widget['widget'])
            widget['widget'].show()
            widget['label'].show()
        for widget in self.followupWidgets:
            self.setTabOrder(oldwidget, widget)
            oldwidget = widget
        if init and self.info is not None:
            for widget in self.additionalFields[self.combo.currentIndex()]:
                widget['widget'].setText(self.info[widget['key']])
        self.modify()

    def modify(self):
        data = {'name': self.getProjectName(), 'system': self.getProjectSystem(),
                'system': self.getProjectSystem(), 'info': self.getProjectInfo()}
        self.setProperty("data", data)
        self.modified.emit()

class NewWindow(QDialog):
    def __init__(self, manager, parent = None, name = "", 
            lang = "", system = 0, info = None):
        super().__init__(parent)
        self.name = name
        self.lang = lang
        self.system = system
        self.info = info
        self.manager = manager
        self.askNew = False
        self.initUI()

    def initUI(self):
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()

        self.cancelbutton = QPushButton(self.tr("Cancel"))
        self.okbutton = QPushButton(self.tr("OK"))
        self.predefinedprojects = PredefinedProjectWidget(self)
        self.advancedproject = AdvancedProjectWidget(self, self.name, self.lang,
                self.system, self.info, [self.okbutton, self.cancelbutton])

        hbox.addWidget(self.predefinedprojects)
        hbox.addWidget(self.advancedproject)

        self.okbutton.setEnabled(False)
        self.cancelbutton.clicked.connect(self.close)
        self.okbutton.clicked.connect(self.ok)
        self.predefinedprojects.currentItemChanged.connect(self.fill)
        self.advancedproject.modified.connect(self.modify)

        hhbox = QHBoxLayout()
        hhbox.addWidget(self.cancelbutton)
        hhbox.addWidget(self.okbutton)
        vbox.addLayout(hbox)
        vbox.addLayout(hhbox)

        self.setLayout(vbox)

        self.modify()

    def ok(self):
        self.askNew = True
        self.close()

    def fill(self):
        item = self.predefinedprojects.currentItem()

        if item is None:
            return

        data = item.data(Qt.UserRole)
        self.advancedproject.fill(data)

    def modify(self):
        self.okbutton.setEnabled(self.advancedproject.isComplete())

    def wantNew(self):
        return self.askNew

    def getProjectName(self):
        return self.advancedproject.getProjectName()

    def getProjectLang(self):
        return self.advancedproject.getProjectLang()

    def getProjectSystem(self):
        return self.advancedproject.getProjectSystem()

    def getProjectInfo(self):
        return self.advancedproject.getProjectInfo()
