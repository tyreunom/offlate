#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ..core.config import *

class SettingsLineEdit(QLineEdit):
    def content(self):
        return self.text()

    def setContent(self, value):
        self.setText(value)

class ListSettingsRowEdit(QDialog):
    def __init__(self, specifications, content, parent = None):
        super().__init__(parent)
        self.content = content
        self.specifications = specifications
        self.widgets = {}
        self.initUI()

    def initUI(self):
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()

        for s in self.specifications:
            edit = QLineEdit()
            edit.setPlaceholderText(self.tr(s.placeholder))
            edit.setText(self.content[s.key])
            self.widgets[s.key] = edit
            hbox.addWidget(edit)

        vbox.addLayout(hbox)

        buttons = QHBoxLayout()
        buttons.addStretch(1)
        okbutton = QPushButton(self.tr("OK"))
        buttons.addWidget(okbutton)
        okbutton.clicked.connect(self.save)
        vbox.addLayout(buttons)
        self.setLayout(vbox)

    def save(self):
        self.content = {}
        for s in self.specifications:
            self.content[s.key] = self.widgets[s.key].text()
        self.close()

class ListSettingsEdit(QWidget):
    textChanged = pyqtSignal()

    def __init__(self, conf, parent = None):
        super(ListSettingsEdit, self).__init__(parent)
        self.conf = conf
        self.initUI()

    def addLine(self, data):
        items = [QTreeWidgetItem(data)]
        self.treeWidget.addTopLevelItems(items)

    def addLineSlot(self):
        d = []
        for i in range(0, len(self.conf.specifications)):
            d.append(self.widgets[i].text())
        items = [QTreeWidgetItem(d)]
        self.treeWidget.addTopLevelItems(items)
        self.textChanged.emit()

    def deleteLineSlot(self):
        self.treeWidget.takeTopLevelItem(self.treeWidget.currentIndex().row())
        self.textChanged.emit()

    def content(self):
        number = self.treeWidget.topLevelItemCount()
        specs = self.conf.specifications
        items = []
        for i in range(0, number):
            item = self.treeWidget.topLevelItem(i)
            data = {}
            j = 0
            for s in specs:
                data[s.key] = item.text(j)
                j += 1
            items.append(data)
        return items

    def setContent(self, data):
        for d in data:
            line = []
            for s in self.conf.specifications:
                line.append(d[s.key])
            self.addLine(line)

    def editLine(self, item, column):
        specs = self.conf.specifications
        data = {}
        i = 0
        for s in specs:
            data[s.key] = item.text(i)
            i += 1

        w = ListSettingsRowEdit(specs, data, self)
        w.exec_()
        i = 0
        for s in specs:
            item.setText(i, w.content[s.key])
            i += 1
        self.textChanged.emit()

    def initUI(self):
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        self.setLayout(vbox)
        self.treeWidget = QTreeWidget()
        self.treeWidget.setColumnCount(len(self.conf.specifications))
        self.treeWidget.itemDoubleClicked.connect(self.editLine)
        vbox.addWidget(self.treeWidget)

        self.widgets = []
        for s in self.conf.specifications:
            edit = QLineEdit()
            edit.setPlaceholderText(self.tr(s.placeholder))
            self.widgets.append(edit)
            hbox.addWidget(edit)

        addbutton = QPushButton(self.tr("Add"))
        addbutton.clicked.connect(self.addLineSlot)
        removebutton = QPushButton(self.tr("Remove"))
        removebutton.clicked.connect(self.deleteLineSlot)

        hbox.addWidget(addbutton)
        hbox.addWidget(removebutton)
        vbox.addLayout(hbox)

class RowSettingsEdit(QWidget):
    textChanged = pyqtSignal()

    def __init__(self, conf, parent = None):
        super(RowSettingsEdit, self).__init__(parent)
        self.conf = conf
        self.data = []
        self.initUI()

    def initUI(self):
        formLayout = QFormLayout()
        self.widgets = {}

        self.row = {}
        self.row[self.conf.indexKey] = self.conf.indexValue
        for s in self.conf.specifications:
            widget = None
            if isinstance(s, StringConfigSpec):
                widget = SettingsLineEdit()
            elif isinstance(s, ListConfigSpec):
                widget = ListSettingsEdit(s)
            elif isinstance(s, RowConfigSpec):
                widget = RowSettingsEdit(s)
            else:
                raise Exception('Unknown spec type ' + str(s))

            try:
                widget.setContent(self.row[s.key])
            except Exception:
                pass
            widget.textChanged.connect(self.update)

            formLayout.addRow(QLabel(self.tr(s.name)), widget)
            label = QLabel(self.tr(s.description))
            label.setWordWrap(True)
            label.setOpenExternalLinks(True)
            formLayout.addRow(label)
            self.widgets[s.key] = widget

        self.setLayout(formLayout)

    def update(self):
        for s in self.conf.specifications:
            widget = self.widgets[s.key]
            value = widget.content()
            self.row[s.key] = value
        self.textChanged.emit()

    def setContent(self, data):
        self.data = data
        for d in data:
            if d[self.conf.indexKey] == self.conf.indexValue:
                self.row = d

    def content(self):
        hasRow = False
        for d in self.data:
            if d[self.conf.indexKey] == self.conf.indexValue:
                hasRow = True

        if hasRow:
            self.data = [self.row if x[self.conf.indexKey] == self.conf.indexValue else x for x in self.data]
        else:
            self.data.append(self.row)

        return self.data
